��    &      L  5   |      P     Q     ^     {     �     �  @   �     �     �     �  *        -     5     I     ]     s     �     �     �  !   �  *   �  :   �     '  '   B  "   j  8   �     �     �     �     �  (        <     A     F     O     V     _     d  ?  i     �  #   �     �     �       G        Z     _     d  0   w     �     �     �     �     �     	     	     	  &   	  ,   F	  K   s	     �	  *   �	  -   
  6   5
     l
     �
     �
  %   �
  3   �
     �
            
        "     0     7     	   !                                $                                                  #                    &   "                                     %   
                             %s found     error connecting by POP3     error sending email %s has the word Cc Connect to %(server)s:%(port)s nick:%(nick)s channel:%(channel)s Date From Give word to: %s Index not in range(0-%(number)d): %(args)s No help Not found any email Not valid index: %s Request word from: %s Start moderating Stop moderating Subject To [core]     load %(type)s %(name)s [core] Not valid config value on log_level [core] can't unsubscribe identifier, it was not subscribed [core] connect to database [core] preparing interfaces and modules [core] restore %s scheduled events [irc %(identifier)s] command received: %(cmd)s: %(args)s args not well formed db request error. display error conecting to server: %s fetching email %(pop_user)s@%(pop_host)s find help moderate remind reminder stop word Project-Id-Version: emma 0.1
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2012-03-01 12:21+0100
PO-Revision-Date: 2012-03-01 12:24+0100
Last-Translator: Ales Zabala Alava <shagi@gisa-elkartea.org>
Language-Team: Basque
Language: eu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
     %s topatuta     errorea POP3 bidez konektatzean     errorea eposta bidaltzean %s-(e)k hitza du Kopia %(server)s:%(port)s-era konektatzen ezizena:%(nick)s kanala:%(channel)s Data Nokr Hitza honek du: %s Indizea ez dago (0-%(number)d) tartean: %(args)s Laguntzarik ez Ez da epostarik topatu Indize ezegokia: %s Hitz eskaria: %s Moderatzen hasi Moderatzeari utzi Gaia Nori [core]     %(type)s %(name)s kargatzen [core] log_level konfigurazio balio ezegokia [core] ezin da identifikadorea ez-harpidetu, ez zegoen lehendik harpidetuta [core] datu basera konektatu [core] interfazeak eta moduluak prestatzen [core] agendatutako %s gertakari berreskuratu [irc %(identifier)s] komandoa jasoa: %(cmd)s: %(args)s argumentu formatu ezegokia db eskaera errorea. erakutsi errorea zerbitzarira konektatzean: %s eposta jasotzeko kontua:  %(pop_user)s@%(pop_host)s bilatu laguntza moderatu gogorarazi gogorarazpena bukatu hitza 