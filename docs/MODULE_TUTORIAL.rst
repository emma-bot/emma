How to add a module to emma
===========================

This short guide explains how to create a new module for emma.

To keep all simple, we'll see how to build the "echo" module, just replying
the given message back.

The code is uploaded to gitorious:
https://gitorious.org/emma/emma-module-echo


Setting up
----------

To begin coding fast and avoid boilerplate code, install paster and some
templates::

    $ pip install PasteScript
    $ pip install ZopeSkel

Then create a new egg for your module. Paster will ask some cuestions and
then create a initial structure ready to install and distribute::

    $ paster create -t nested_namespace emma.module.echo
    Expert Mode? (What question mode would you like? (easy/expert/all)?) ['easy']:
    Version (Version number for project) ['1.0']:
    Description (One-line description of the project) ['']: Echo module for emma
    ...

Enter in the newly created directory and fix some files: add yourself to
contributors, write the README, etc.

    $ cd emma.module.echo
    $ vim CONTRIBUTORS.txt # put yourself
    $ vim README.txt # Add at least a one line describing the module
    $ vim setup.py # Fix the url


Writting the module
-------------------

Time to start coding! All the code will live in
emma.module.echo/src/emma/module/echo/__init__.py

First, the module must have a Module subclass with the same name as the
module. The only required method for Module is run, to setup the module
when emma starts::

    >>> from emma.module import Module
    >>>
    >>> class echo(Module):
    >>>     def run(self):
    >>>         help_event = Event(event="help", identifier=self.conf['id'])
    >>>         subscribe(help_event, self.help_handler)
    >>>         cmd_event = Event(event="command", identifier=self.conf['id'])
    >>>         subscribe(cmd_event, self.cmd_handler)

Note how this module listens to "help" and "command" events. When any of
then happen, the associated handler is executed.

Next, we'll write help_handler. This way the module will appear in the interactive
help::

    >>>     def help_handler(self, event, data):
    >>>         if not data:
    >>>             return " Echo something back.\n" \
    >>>                     " * echo message\n" \
    >>>                     "   says message."
    >>>          elif data[0] in ('echo', _('echo')):
    >>>              return "Repeat the given message"

Last, we'll write the command handler::

    >>>     def cmd_handler(self, event, data):
    >>>         cmd, args = data[0]
    >>>         if cmd in ('echo', _('echo')):
    >>>             to = data[1]['From']
    >>>             if event.interface == 'irc' and data[1]['To'][0] == '#':
    >>>                 to = data[1]['To']
    >>>
    >>>             msg = Message(args, to)
    >>>             event = Event(event="send", interface=event.interface,
    >>>                           identifier=event.identifier)
    >>>             trigger(event, msg)

Note event and data information is queried and how Message and Event are
instantiated to give a response.


Adding to the config and running
--------------------------------

The code is already written, just install it with setup.py::

    $ python setup.py install

Add the new module to emma.cfg::
    [M echo bar]
    id = foo

And ready to run! Now emma should be able to reply to echo requests :-)
Remember to restart emma if already running.
